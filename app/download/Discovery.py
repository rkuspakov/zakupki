from app.download.DiscoveryDocs import DiscoveryDocs
from app.download.DiscoveryDict import DiscoveryDict
from app.download.FilesResearch import FilesResearch
from app.download.MoveFiles import MoveFiles
import datetime


class Discovery:

    def __init__(self):
        super().__init__()

    def discovery(self):
        a = DiscoveryDocs('')
        threadsa = a.create_threads()
        for thread in threadsa:
            thread.join()
        a.completedict()


if __name__ == "__main__":
    print(datetime.datetime.now())
    a = Discovery()
    a.discovery()
    print(datetime.datetime.now())