import os
import re
import ftplib
import datetime
from threading import Thread
from app.config.Config import Config
from base.Connection import Connection
from app.fcs_nsi.TypeDict import TypeDict


class DiscoveryDict(Thread):
    """
    A threading example
    """

    def __init__(self, typedict):
        """Инициализация потока"""
        Thread.__init__(self)
        self.typedict = typedict

    def run(self):
        """Запуск потока"""
        # перебираем список всех типов справочников
        # уведомление о начале скачивания
        print("Обнаружение: " + self.typedict)
        fz = '44'
        root = 'fcs_nsi'
        region = 'Moskva'

        try:
            with Connection() as db:
                print("data\\" + root + "\\" + self.typedict)
                # ftp://free:free@ftp.zakupki.gov.ru
                # Соединяемся с FTP-сервером
                host = 'ftp.zakupki.gov.ru'
                ftp_user = 'free'
                ftp_password = 'free'

                # каждую переменную подключим к авторизации:
                ftp = ftplib.FTP(host, ftp_user, ftp_password)
                # welcome_text = ftp.getwelcome()
                # print(welcome_text)  # Вывели на экран Welcome-сообщение сервера

                # навигация по папкам
                ftp.cwd(root)
                ftp.cwd(self.typedict)


                listing = []
                ftp.retrlines("LIST", listing.append)

                for f in listing:
                    words = f.split(None, 8)
                    filename = words[-1]
                    filesize = words[4]
                    dposition = str(words[6])
                    dtime = str(words[7])

                    basedir = Config.BASE_DIR['BASEDIR'] + root + "\\" + self.typedict
                    local_filename = os.path.join(basedir, filename)

                    date_regex = re.compile(r'_(\d+)')
                    date_matches = re.findall(date_regex, filename)

                    month = str(words[5])
                    datastart = date_matches[0][:4] + "-" + date_matches[0][4:6] + "-" + date_matches[0][6:8]
                    dataend = None
                    count = date_matches[-1]

                    # запись в базу данных
                    db.execute("""insert into tempdownload (
                                                        fz, root, region, typedoc, month, filename, filesize, dposition, dtime, datastart, dataend, count, path
                                                        ) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (
                        fz, root, region, self.typedict, month, filename, filesize, dposition, dtime, datastart,
                        dataend, count, local_filename
                    ))

                ftp.close()
                db.commit()
        except Exception as e:
            print('Ошибка: ' + str(e) + ' в файле: ' + local_filename)
            # pass


    def completedict(self):
        with Connection() as db:
            # удаляем записи, которые уже есть во временной таблице
            db.execute("delete from tempdownload where id in (select id from tempdownload where path in ("
                       "select path from downloadfiles));")

            # переносим все новые записи в рабочую таблицу
            db.execute(
                "insert into downloadfiles (fz, root, region, typedoc, month, filename, filesize, dposition, dtime, "
                "datastart, dataend, count, path) select fz, root, region, typedoc, month, filename, filesize, "
                "dposition, dtime, datastart, dataend, count, path from tempdownload;")

            # удаляем все записи из новой таблицы
            db.execute("delete from tempdownload where id in (select id from tempdownload where path in ("
                       "select path from downloadfiles));")
            db.commit()

    def create_threads(self):
        """
        Запускаем программу
        """
        threads = []

        # получаем список всех справочников
        typedict = TypeDict()
        print(datetime.datetime.now())

        # перебираем список всех справочников
        for row in typedict:
            print("Поток %s" % row)
            thread = DiscoveryDict(row)
            thread.start()
            threads.append(thread)
            # break

        return threads


if __name__ == "__main__":
    c = DiscoveryDict('')
    threads = c.create_threads()
    for thread in threads:
        thread.join()
    c.completedict()
