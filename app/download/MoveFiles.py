# -*- coding: utf-8 -*-
import os
import re
import datetime
from app.config.Config import Config
from app.fcs_regions.TypeDocs import TypeDocs
from base.Connection import Connection


class MoveFiles:

    def __init__(self, region):
        self.region = region

    def movefiles(self):
        """Запуск потока"""
        # перемещение файлов из папки currMonth в папку prevMonth
        root = 'fcs_regions'
        typedoc = TypeDocs()
        place = ['currMonth', ]

        try:
            with Connection() as db:
                for i in typedoc:
                    for j in place:
                        # получаем целевую папку
                        basedir = Config.BASE_DIR['BASEDIR'] + root + "\\" + self.region + "\\" + i + "\\" + j

                        # Получаем список файлов в целевой папке
                        for root, dirs, files in os.walk(basedir):
                            for name in files:
                                date_regex = re.compile(r'(\d{4}\d{2}\d{2})')
                                date_matches = re.findall(date_regex, name)
                                datastart = datetime.datetime.strptime(date_matches[0], "%Y%m%d").date()

                                # переместить файл если ему больше месяца
                                deltadays = datetime.date.today() - datastart
                                if deltadays.days > 30:
                                    FolderOne = basedir + '\\' + name
                                    FolderSecond = (basedir + '\\' + name).replace("currMonth", "prevMonth")
                                    os.replace(FolderOne, FolderSecond)
                                    # print('Перемещение: ' + basedir + '\\' + name)

        except Exception as e:
            print('Ошибка: ' + str(e) + ' перемещение файлов в папке: ' + basedir)

        # переместить устаревшие файлы в базе
        self.movefiles_indb()

    def movefiles_indb(self):
        try:
            with Connection() as db:
                # получение записей в базе
                db.execute("CALL public.proc_movefiles()")
        except Exception as e:
            print('Ошибка: ' + str(e) + '  изменения записей в базе')

    def delfiles(self):
        """Запуск потока"""
        # удаление файлов из папки prevMonth
        root = 'fcs_regions'
        typedoc = TypeDocs()
        place = ['prevMonth', ]

        try:
            with Connection() as db:
                for i in typedoc:
                    for j in place:
                        # получаем целевую папку
                        basedir = Config.BASE_DIR['BASEDIR'] + root + "\\" + self.region + "\\" + i + "\\" + j

                        # Получаем список файлов в целевой папке
                        for root, dirs, files in os.walk(basedir):
                            for name in files:
                                date_regex = re.compile(r'(\d{4}\d{2}\d{2})')
                                date_matches = re.findall(date_regex, name)
                                datastart = datetime.datetime.strptime(date_matches[0], "%Y%m%d").date()

                                # удалить файл если ему больше двух месяцев
                                deltadays = datetime.date.today() - datastart
                                if deltadays.days > 60:
                                    os.remove(basedir + '\\' + name)
                                    print('Удаление: ' + basedir + '\\' + name)

        except Exception as e:
            print('Ошибка: ' + str(e) + ' удаления файлов в папке: ' + basedir)

        # удалить устаревшие записи в базе
        self.delfiles_indb()

    def delfiles_indb(self):
        try:
            with Connection() as db:
                # получение записей в базе
                db.execute("CALL public.proc_delfiles()")
        except Exception as e:
            print('Ошибка: ' + str(e) + ' удаления записей в базе')

    def moves(self):
        # получаем список всех регионов
        with Connection() as db:
            db.execute('select name from pathdirectory;')
            records = db.fetchall()

            print(datetime.datetime.now())

            # перебираем список всех регионов
            for row in records:
                self.region = row[0]
                self.delfiles()
                self.movefiles()


if __name__ == "__main__":
    m = MoveFiles('')
    m.moves()
