# -*- coding: utf-8 -*-
import datetime
import os
from threading import Thread
from base.Connection import Connection


class FilesResearch(Thread):
    """
    A threading example
    """

    def __init__(self, region):
        """Инициализация потока"""
        Thread.__init__(self)
        self.region = region

    def run(self):
        """Запуск потока"""
        lfa = self.getListFilesForAnalisys()
        with Connection() as db:
            for i in lfa:
                try:
                    filename = str(i[1])
                    result = os.path.exists(filename)

                    if result:
                        # запись в базу данных
                        db.execute("UPDATE DownloadFiles SET dataresearch = '" + str(datetime.datetime.now()) +
                                   "', datadownload = '" + str(datetime.datetime.now()) +
                                   "' WHERE id = '" + str(i[0]) + "';")
                except Exception as e:
                    print('Ошибка: ' + str(e) + ' в файле: ' + filename)
                    # pass
        db.commit()

        print("Регион: " + self.region + " - " + str(datetime.datetime.now()))

    def getListFilesForAnalisys(self):
        with Connection() as db:
            # проверка наличия записи в базе
            db.execute(
                "SELECT id, Path FROM DownloadFiles WHERE region = '" + self.region + "' and dataresearch is Null;")
            result = db.fetchall()

        return result

    def create_threads(self):
        """
        Запускаем программу
        """
        threads = []

        # получаем список всех регионов
        with Connection() as db:
            db.execute('select name from pathdirectory;')
            records = db.fetchall()

            print(datetime.datetime.now())

            # перебираем список всех регионов
            for row in records:
                # print("Поток %s" % row[0])
                thread = FilesResearch(row[0])
                thread.start()
                threads.append(thread)
                # break

        return threads


if __name__ == "__main__":
    c = FilesResearch('')
    threads = c.create_threads()
    for thread in threads:
        thread.join()
