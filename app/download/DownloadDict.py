# -*- coding: utf-8 -*-
import os, ftplib, datetime
from threading import Thread
from app.config.Config import Config
from base.Connection import Connection
from app.fcs_nsi.TypeDict import TypeDict


class DownloadDict(Thread):
    """
        A threading example
    """

    def __init__(self, typedict):
        """Инициализация потока"""
        Thread.__init__(self)
        self.typedict = typedict

    def run(self):
        """Запуск потока"""
        # перебираем список всех справочников
        # уведомление о начале скачивания
        print("Скачивание: " + self.typedict + "\n")
        fz = '44'
        root = 'fcs_nsi'
        region = 'Moskva'

        try:
            with Connection() as db:
                db.execute("""select path, filename from public.downloadfiles where root = 'fcs_nsi' 
                              and typedoc = '""" + self.typedict + """';""")
                records = db.fetchall()

                print(datetime.datetime.now())

                # перебираем список всех регионов
                for row in records:
                    local_filename = row[0]
                    filename = row[1]

                    if os.path.isfile(row[0]) != True:
                        # ftp://free:free@ftp.zakupki.gov.ru
                        # Соединяемся с FTP-сервером
                        host = 'ftp.zakupki.gov.ru'
                        ftp_user = 'free'
                        ftp_password = 'free'

                        # каждую переменную подключим к авторизации:
                        ftp = ftplib.FTP(host, ftp_user, ftp_password)
                        # welcome_text = ftp.getwelcome()
                        # print(welcome_text)  # Вывели на экран Welcome-сообщение сервера

                        # навигация по папкам
                        ftp.cwd(root)
                        ftp.cwd(self.typedict)

                        basedir = Config.BASE_DIR['BASEDIR']

                        if not os.path.isdir(basedir + root):
                            os.mkdir(basedir + root)
                        if not os.path.isdir(basedir + root + "\\" + self.typedict):
                            os.mkdir(basedir + root + "\\" + self.typedict)

                        print("Скачиваем: " + local_filename + "\n")

                        # скачиваем файл
                        lf = open(local_filename, "wb")
                        ftp.retrbinary("RETR " + filename, lf.write, 8 * 1024)
                        lf.close()

        except Exception as e:
            print('Ошибка: ' + str(e) + ' в файле: ' + local_filename)
            # pass

    def create_threads(self):
        """
            Запускаем программу
        """
        threads = []

        # получаем список всех справочников
        typedict = TypeDict()
        print(datetime.datetime.now())

        # перебираем список всех справочников
        for row in typedict:
            # print("Поток %s" % row + "\n")
            thread = DownloadDict(row)
            thread.start()
            threads.append(thread)
            # break

        return threads


if __name__ == "__main__":
    c = DownloadDict('')
    threads = c.create_threads()
    for thread in threads:
        thread.join()
