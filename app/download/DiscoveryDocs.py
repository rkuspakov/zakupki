import os
import re
import ftplib
import datetime
from pathlib import Path
from threading import Thread
from app.config.Config import Config
from base.Connection import Connection
from app.fcs_regions.TypeDocs import TypeDocs


class DiscoveryDocs(Thread):
    """
    A threading example
    """

    def __init__(self, region):
        """Инициализация потока"""
        Thread.__init__(self)
        self.region = region

    def run(self):
        """Запуск потока"""
        # уведомление о начале скачивания
        # print("Скачивание: " + self.region)
        fz = '44'
        root = 'fcs_regions'
        typedoc = TypeDocs()
        place = ['currMonth', 'prevMonth', '']

        try:
            with Connection() as db:
                for i in typedoc:
                    for j in place:
                        # print("data\\" + root + "\\" + self.region + "\\" + i + "\\" + j)
                        # ftp://free:free@ftp.zakupki.gov.ru
                        # Соединяемся с FTP-сервером
                        host = 'ftp.zakupki.gov.ru'
                        ftp_user = 'free'
                        ftp_password = 'free'

                        # каждую переменную подключим к авторизации:
                        ftp = ftplib.FTP(host, ftp_user, ftp_password)
                        # welcome_text = ftp.getwelcome()
                        # print(welcome_text)  # Вывели на экран Welcome-сообщение сервера

                        # навигация по папкам
                        ftp.cwd(root)
                        ftp.cwd(self.region)
                        ftp.cwd(i)
                        if j != '':
                            ftp.cwd(j)

                        listing = []
                        ftp.retrlines("LIST", listing.append)

                        for f in listing:
                            words = f.split(None, 8)
                            filename = words[-1]
                            if filename not in place and Path(filename).suffix in ('.zip'):
                                filesize = words[4]
                                dposition = str(words[6])
                                dtime = str(words[7])

                                if j == '':
                                    basedir = Config.BASE_DIR['BASEDIR'] +  root + "\\" + self.region + "\\" + i
                                else:
                                    basedir = Config.BASE_DIR['BASEDIR'] + root + "\\" + self.region + "\\" + i + "\\" + j

                                local_filename = os.path.join(basedir, filename)

                                date_regex = re.compile(r'_(\d+)')
                                date_matches = re.findall(date_regex, filename)

                                typedoc = filename.split('_')[0]
                                month = str(words[5])
                                datastart = date_matches[0][:4] + "-" + date_matches[0][4:6] + "-" + date_matches[0][6:8]
                                dataend = date_matches[1][:4] + "-" + date_matches[1][4:6] + "-" + date_matches[1][6:8]
                                count = date_matches[-1]

                                # запись в базу данных
                                db.execute("""insert into tempdownload (
                                                fz, root, region, typedoc, month, filename, filesize, dposition, dtime, datastart, dataend, count, path
                                                ) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (
                                                fz, root, self.region, typedoc, month, filename, filesize, dposition, dtime, datastart,
                                                dataend, count, local_filename
                                            ))
                            elif filename not in place and Path(filename).suffix not in ('.zip'):
                                pass
                        ftp.close()
                db.commit()
        except Exception as e:
            if e.args[0].find('Entering Passive Mode') < 0:
                print('Ошибка: ' + str(e) + ' в файле: ' + local_filename)
            # pass

    def completedict(self):
        with Connection() as db:
            # удаляем записи, которые уже есть во временной таблице
            db.execute("delete from tempdownload where id in (select id from tempdownload where path in ("
                       "select path from downloadfiles));")

            # переносим все новые записи в рабочую таблицу
            db.execute(
                "insert into downloadfiles (fz, root, region, typedoc, month, filename, filesize, dposition, dtime, "
                "datastart, dataend, count, path) select fz, root, region, typedoc, month, filename, filesize, "
                "dposition, dtime, datastart, dataend, count, path from tempdownload;")

            # удаляем все записи из новой таблицы
            db.execute("delete from tempdownload where id in (select id from tempdownload where path in ("
                       "select path from downloadfiles));")
            db.commit()

    def create_threads(self):
        """
        Запускаем программу
        """

        threads = []

        # получаем список всех регионов
        with Connection() as db:
            db.execute('select name from pathdirectory;')
            records = db.fetchall()

            print(datetime.datetime.now())

            # перебираем список всех регионов
            for row in records:
                # print("Поток %s" % row[0])+
                thread = DiscoveryDocs(row[0])
                thread.start()
                threads.append(thread)
                # break
        return threads


if __name__ == "__main__":
    c = DiscoveryDocs('')
    threads = c.create_threads()
    for thread in threads:
        thread.join()
    c.completedict()
