from app.download.DownloadDict import DownloadDict
from app.download.DownloadDocs import DownloadDocs
import datetime

class Download:

    def __init__(self):
        super().__init__()

    def download(self):
        a = DownloadDict('')
        threadsa = a.create_threads()
        for thread in threadsa:
            thread.join()

        b = DownloadDocs('')
        threadsb = b.create_threads()
        for thread in threadsb:
            thread.join()


if __name__ == "__main__":
    print(datetime.datetime.now())
    a = Download()
    a.download()
    print(datetime.datetime.now())