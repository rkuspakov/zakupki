# -*- coding: utf-8 -*-
import os, ftplib, datetime
from threading import Thread
from app.config.Config import Config
from base.Connection import Connection
from app.fcs_regions.TypeDocs import TypeDocs


class DownloadDocs(Thread):
    """
        A threading example
    """

    def __init__(self, typedocs):
        """Инициализация потока"""
        Thread.__init__(self)
        self.typedocs = typedocs

    def run(self):
        """Запуск потока"""
        # перебираем список всех справочников
        # уведомление о начале скачивания
        print("Скачивание: " + self.typedocs + "\n")
        fz = '44'
        root = 'fcs_regions'
        region = ''

        try:
            with Connection() as db:
                db.execute("""select path, filename, region from public.downloadfiles where root = 'fcs_regions' 
                              and typedoc = '""" + self.typedocs + """';""")
                records = db.fetchall()

                print(datetime.datetime.now())

                # перебираем список всех регионов
                for row in records:
                    local_filename = row[0]
                    filename = row[1]
                    region = row[2]

                    if os.path.isfile(row[0]) != True:
                        # ftp://free:free@ftp.zakupki.gov.ru
                        # Соединяемся с FTP-сервером
                        host = 'ftp.zakupki.gov.ru'
                        ftp_user = 'free'
                        ftp_password = 'free'

                        # каждую переменную подключим к авторизации:
                        ftp = ftplib.FTP(host, ftp_user, ftp_password)
                        # welcome_text = ftp.getwelcome()
                        # print(welcome_text)  # Вывели на экран Welcome-сообщение сервера

                        # навигация по папкам
                        ftp.cwd(root)
                        ftp.cwd(region)
                        ftp.cwd(self.typedocs)

                        basedir = Config.BASE_DIR['BASEDIR']

                        if not os.path.isdir(basedir + root):
                            os.mkdir(basedir + root)
                        if not os.path.isdir(basedir + root + "\\" + region):
                            os.mkdir(basedir + root + "\\" + region)
                        if not os.path.isdir(basedir + root + "\\" + region + "\\" + self.typedocs):
                            os.mkdir(basedir + root + "\\" + region + "\\" + self.typedocs)

                        if not os.path.isdir(basedir + root + "\\" + region + "\\" + self.typedocs + "\\prevMonth"):
                            os.mkdir(basedir + root + "\\" + region + "\\" + self.typedocs + "\\prevMonth")
                        if not os.path.isdir(basedir + root + "\\" + region + "\\" + self.typedocs + "\\currMonth"):
                            os.mkdir(basedir + root + "\\" + region + "\\" + self.typedocs + "\\currMonth")

                        print("Скачиваем: " + local_filename + "\n")

                        # скачиваем файл
                        if not os.path.exists(local_filename):
                            try:
                                lf = open(local_filename, "wb")
                                ftp.retrbinary("RETR " + filename, lf.write, 8 * 1024)
                            except:
                                pass
                            finally:
                                lf.close()

        except Exception as e:
            print('Ошибка: ' + str(e) + ' в файле: ' + local_filename)
            # pass

    def create_threads(self):
        """
            Запускаем программу
        """
        threads = []

        # получаем список всех документов
        typedocs = TypeDocs()
        print(datetime.datetime.now())

        # перебираем список всех документов
        for row in typedocs:
            # print("Поток %s" % row + "\n")
            thread = DownloadDocs(row)
            thread.start()
            threads.append(thread)
            # break

        return threads


if __name__ == "__main__":
    c = DownloadDocs('')
    threads = c.create_threads()
    for thread in threads:
        thread.join()



