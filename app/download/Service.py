from app.download.FilesResearch import FilesResearch
from app.download.MoveFiles import MoveFiles
import datetime


class Service:

    def __init__(self):
        super().__init__()

    def service(self):
        c = FilesResearch('')
        threadsc = c.create_threads()
        for thread in threadsc:
            thread.join()

        m = MoveFiles('')
        m.moves()


if __name__ == "__main__":
    print(datetime.datetime.now()) 
    a = Service()
    a.service()
    print(datetime.datetime.now()) 