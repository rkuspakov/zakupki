class TypeDocs(object):
    def __init__(self):
        self._data = ['addinfo', 'auditresult', 'ContractProcedureDocs', 'contractprojects', 'contracts',
                      'customerreports', 'customerreports', 'notificationExceptions', 'notifications',
                      'plangraphs', 'plangraphs2017', 'plangraphs2020', 'protocols', 'protocolsfrequnl',
                      'purchasedocs', 'purchaseplans', 'regulationrules', 'requestquotation', 'sketchplans', ]

    def __iter__(self):
        for elem in self._data:
            yield elem


if __name__ == "__main__":

    a = TypeDocs()
    for elem in a:
        print(elem)
