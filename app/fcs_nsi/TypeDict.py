class TypeDict(object):
    def __init__(self):
        self._data = ['nsiCurrency', 'nsiContractCurrencyCBRF', 'nsiOrganization', 'nsiOKEI', 'nsiOKPD2', 'nsiOKTMO', 'nsiOKVED2', 'nsiPlacingWay', 'nsiKTRU']

    def __iter__(self):
        for elem in self._data:
            yield elem


if __name__ == "__main__":

    a = TypeDict()
    for elem in a:
        print(elem)
