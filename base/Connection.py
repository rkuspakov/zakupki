import psycopg2
from app.config.Config import Config


class Connection:

    def __init__(self):
        self._conn = psycopg2.connect(user=Config.DATABASE_CONFIG['user'],
                                      password=Config.DATABASE_CONFIG['password'],
                                      host=Config.DATABASE_CONFIG['host'],
                                      port=Config.DATABASE_CONFIG['port'],
                                      database=Config.DATABASE_CONFIG['database'])
        self._cursor = self._conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    @property
    def connection(self):
        return self._conn

    @property
    def cursor(self):
        return self._cursor

    def commit(self):
        self.connection.commit()

    def close(self, commit=True):
        if commit:
            self.commit()
        self.connection.close()

    def execute(self, sql, params=None):
        self.cursor.execute(sql, params or ())

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def query(self, sql, params=None):
        self.cursor.execute(sql, params or ())
        return self.fetchall()

    @property
    def getRegionList(self):
        return self.query('SELECT name FROM PathDirectory;')

    def transactions_by_date(self, date):
        sql = "SELECT * FROM PathDirectory;"
        return self.query(sql, (date,))


if __name__ == '__main__':
    with Connection() as db:
        db.execute('select name from pathdirectory;')
        records = db.fetchall()
