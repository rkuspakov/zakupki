-- PROCEDURE: public.proc_delfiles()

-- DROP PROCEDURE IF EXISTS public.proc_delfiles();

CREATE OR REPLACE PROCEDURE public.proc_delfiles(
	)
LANGUAGE 'sql'
AS $BODY$
DELETE FROM public.downloadfiles
where date_trunc('day', NOW()-datastart) > '56 days'
and root = 'fcs_regions'
and (path like '%currMonth%' or path like '%prevMonth%')
$BODY$;
ALTER PROCEDURE public.proc_delfiles()
    OWNER TO postgres;

