-- Table: public.pathdirectory

-- DROP TABLE IF EXISTS public.pathdirectory;

CREATE TABLE IF NOT EXISTS public.pathdirectory
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name text COLLATE pg_catalog."default",
    isdelete boolean,
    datadownload timestamp without time zone,
    dataresearch timestamp without time zone,
    dataextract timestamp without time zone,
    CONSTRAINT pathdirectory_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.pathdirectory
    OWNER to postgres;

-- Добавление списка регионов

INSERT INTO public.pathdirectory(name)  VALUES ('Adygeja_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Altaj_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Altajskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Amurskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Arkhangelskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Astrakhanskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Bajkonur_g');
INSERT INTO public.pathdirectory(name)  VALUES ('Bashkortostan_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Belgorodskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Brjanskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Burjatija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Chechenskaja_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Cheljabinskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Chukotskij_AO');
INSERT INTO public.pathdirectory(name)  VALUES ('Chuvashskaja_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Dagestan_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Evrejskaja_Aobl');
INSERT INTO public.pathdirectory(name)  VALUES ('Ingushetija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Irkutskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Ivanovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Jamalo-Neneckij_AO');
INSERT INTO public.pathdirectory(name)  VALUES ('Jaroslavskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Kabardino-Balkarskaja_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Kaliningradskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Kalmykija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Kaluzhskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Kamchatskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Karachaevo-Cherkesskaja_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Karelija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Kemerovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Khabarovskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Khakasija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Khanty-Mansijskij_AO-Jugra_AO');
INSERT INTO public.pathdirectory(name)  VALUES ('Kirovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Komi_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Kostromskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Krasnodarskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Krasnojarskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Krim_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Kurganskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Kurskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Leningradskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Lipeckaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Magadanskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Marij_El_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Mordovija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Moskovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Moskva');
INSERT INTO public.pathdirectory(name)  VALUES ('Murmanskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Neneckij_AO');
INSERT INTO public.pathdirectory(name)  VALUES ('Nizhegorodskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Novgorodskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Novosibirskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Omskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Orenburgskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Orlovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Penzenskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Permskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Primorskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Pskovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Rjazanskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Rostovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Sakha_Jakutija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Sakhalinskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Samarskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Sankt-Peterburg');
INSERT INTO public.pathdirectory(name)  VALUES ('Saratovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Sevastopol_g');
INSERT INTO public.pathdirectory(name)  VALUES ('Severnaja_Osetija-Alanija_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Smolenskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Stavropolskij_kraj');
INSERT INTO public.pathdirectory(name)  VALUES ('Sverdlovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Tambovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Tatarstan_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Tjumenskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Tomskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Tulskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Tverskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Tyva_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Udmurtskaja_Resp');
INSERT INTO public.pathdirectory(name)  VALUES ('Uljanovskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Vladimirskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Volgogradskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Vologodskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Voronezhskaja_obl');
INSERT INTO public.pathdirectory(name)  VALUES ('Zabajkalskij_kraj');