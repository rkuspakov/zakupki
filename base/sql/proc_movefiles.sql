-- PROCEDURE: public.proc_movefiles()

-- DROP PROCEDURE IF EXISTS public.proc_movefiles();

CREATE OR REPLACE PROCEDURE public.proc_movefiles(
	)
LANGUAGE 'sql'
AS $BODY$
UPDATE public.downloadfiles
SET path = replace(path, 'currMonth', 'prevMonth')
where date_trunc('day', NOW()-datastart) > '30 days'
$BODY$;
ALTER PROCEDURE public.proc_movefiles()
    OWNER TO postgres;
