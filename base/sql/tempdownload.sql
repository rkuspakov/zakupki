-- Table: public.tempdownload

-- DROP TABLE IF EXISTS public.tempdownload;

CREATE TABLE IF NOT EXISTS public.tempdownload
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    fz character varying(3) COLLATE pg_catalog."default",
	root character varying(15) COLLATE pg_catalog."default",
    region text COLLATE pg_catalog."default",
    typedoc text COLLATE pg_catalog."default",
    month text COLLATE pg_catalog."default",
    filename text COLLATE pg_catalog."default",
    filesize double precision,
    dtime character varying COLLATE pg_catalog."default",
    dposition character varying COLLATE pg_catalog."default",
    datastart timestamp with time zone,
    dataend timestamp with time zone,
    count integer,
    path text COLLATE pg_catalog."default",
    "extract" timestamp with time zone,
    dataresearch timestamp with time zone,
    CONSTRAINT tempdownload_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tempdownload
    OWNER to postgres;