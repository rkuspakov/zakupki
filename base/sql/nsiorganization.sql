-- Table: public.nsiorganization

-- DROP TABLE IF EXISTS public.nsiorganization;

CREATE TABLE IF NOT EXISTS public.nsiorganization
(
    regnumber character varying COLLATE pg_catalog."default" NOT NULL,
    inn character varying COLLATE pg_catalog."default",
    kpp character varying COLLATE pg_catalog."default",
    shortname text COLLATE pg_catalog."default",
    fullname text COLLATE pg_catalog."default",
    CONSTRAINT nsiorganization_pkey PRIMARY KEY (regnumber)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.nsiorganization
    OWNER to postgres;